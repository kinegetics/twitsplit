//
//  TwitSplitTests.swift
//  TwitSplitTests
//
//  Created by Richard Yip on 3/10/18.
//  Copyright © 2018 Richard Yip. All rights reserved.
//

import XCTest
@testable import TwitSplit

class TwitSplitTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLessThan50Char() {
        
        let message = "I can't believe Tweeter now supports chunking my"
        
        MessageService.splitMessage(text: message) { (result, error) in
            let text = result?.first
            XCTAssertEqual(message, text, "input message not equal to output message")
        }
      
    }
    
    
    func testNonWwhitespaceCharactersLongerThan50Characters() {
        
        let message = "I can't believe Tweeter now 123456789012345678901234567890123456789012345678901234567890 chunking my"
        
        MessageService.splitMessage(text: message) { (result, error) in
            let text = result?.first
        
            XCTAssert(error == MessageError.nonWhiteSpaceExceedLimit, (text ?? nil) ?? "")
        }
    }
    
    func testSplitIntoTwoMessages() {
        
        let message =
        "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself."
        
        MessageService.splitMessage(text: message) { (result, error) in
            
            let first = result?[0]
//            debugPrint("first \(String(describing: first))")
            let second = result?[1]
//            debugPrint("second \(String(describing: second))")
            
            XCTAssert(first == "1/2 I can't believe Tweeter now supports chunking" && second == "2/2 my messages, so I don't have to do it myself.")
        }
    }
    
    func testSplitLongMessage() {

        let message =
        "This should be quite a long text and maybe it should be that long again and also not like it so again one more time this must be long and long enough to test This should be quite a long text and maybe it should be that long again and also not like it so again one more time this must be long and long enough to test This should be quite a long text and maybe it should be that long again and also not like it so again one more time this must be long and long enough to test."

        MessageService.splitMessage(text: message) { (result, error) in

            let items = result!
            XCTAssert(
                items[0] == "1/11 This should be quite a long text and maybe it"
                && items[1] == "2/11 should be that long again and also not like"
                && items[2] == "3/11 it so again one more time this must be long"
                && items[3] == "4/11 and long enough to test This should be quite"
                && items[4] == "5/11 a long text and maybe it should be that long"
                && items[5] == "6/11 again and also not like it so again one more"
                && items[6] == "7/11 time this must be long and long enough to"
                && items[7] == "8/11 test This should be quite a long text and"
                && items[8] == "9/11 maybe it should be that long again and also"
                && items[9] == "10/11 not like it so again one more time this must"
                && items[10] == "11/11 be long and long enough to test."
            )
        }
    }
}
