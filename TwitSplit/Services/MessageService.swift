//
//  MessageService.swift
//  TwitSplit
//
//  Created by Richard Yip on 3/10/18.
//  Copyright © 2018 Richard Yip. All rights reserved.
//

import Foundation

enum MessageError: Error {
    case nonWhiteSpaceExceedLimit
    case unableToSplitText
    case unkownError
}

final class MessageService {
    
    static func splitMessage(text: String, _ completion:@escaping ([String]?, MessageError?) ->())  {
    
        var result:[String] = [String]()
        
        // less than 50 char, return text
        guard text.count > 50 else {
            result.append(text)
            completion(result, nil)
            return
        }
        
        //  check for non-whitespace characters longer than 50 characters
        let array = text.components(separatedBy: " ")
        for item in array {
            if item.count > 50 {
                result.append(item)
                completion(result, .nonWhiteSpaceExceedLimit)
                return
            }
        }
        
        var count = (Double(text.count) / 50.0) + 1.0
        var chunks = Int(count)
        
        var displayText = String()
        displayText.append(contentsOf: text)
        
        for index in 1...chunks {
            var prefix:String
            prefix = String(format: "%d/%d ", index, chunks)
            displayText.append(contentsOf: prefix)
        }
        
        count = (Double(displayText.count) / 50.0) + 1.0
        chunks = Int(count)
        
        var counter = 1
        var message:String = (String(format: "%d/%d %@", counter, chunks, text))

        var indexStartOfText = message.startIndex
        var indexEndOfText = message.index(message.startIndex, offsetBy: 50)
        
        repeat {
           
            var subString = String(message[indexStartOfText..<indexEndOfText])
            
            let nextMessage = message[indexEndOfText..<message.endIndex]
            if nextMessage.first != " " && subString.last != " " {
                repeat {
                    indexEndOfText = message.index(before: indexEndOfText)
                    subString = String(message[indexStartOfText..<indexEndOfText])
                    
                } while subString.last != Character.init(" ")
            }
            else {
                subString = String(message[indexStartOfText..<indexEndOfText])
            }
  
            subString = subString.trimmingCharacters(in: NSCharacterSet.whitespaces)
//            debugPrint("subString \(subString)")
            result.append(String(subString))
            
            counter = counter + 1
            message = String(message.dropFirst(subString.count))
            message = message.trimmingCharacters(in: NSCharacterSet.whitespaces)
            message = String(format: "%d/%d %@", counter, chunks, message)
            
            if counter == Int(count) {
                result.append(String(message))
                break
            }
            indexStartOfText = message.startIndex
            indexEndOfText = message.index(message.startIndex, offsetBy: 50)
            
     
        } while indexEndOfText <= message.endIndex
        
//        debugPrint(text)
//
//        for item in result {
//            debugPrint(item)
//        }
        
        completion(result, nil)
    }
}
